using UnityEngine;
using System.Collections;

public class SavePointTrigger : MonoBehaviour {
	
	int trigger = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "hero")
		{
			GlobalAttributes.PathSpawn = 0;
			trigger++;
			
			if(trigger == 2)
			{
				OnScean.LitFireFlag = 0;
				trigger = 0;
			}
		}
	}
}
