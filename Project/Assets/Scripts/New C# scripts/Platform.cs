using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnCollisionEnter(Collision coll)
	{
		if(coll.gameObject.tag == "enemybullet" || coll.gameObject.tag == "herobullet" || coll.gameObject.tag == "secherobullet" || coll.gameObject.tag == "SurprizeDeamon")
		{
			Destroy(coll.gameObject);
		}
	}
}
