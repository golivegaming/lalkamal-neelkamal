using UnityEngine;
using System.Collections;

public class DayandNightEffect : MonoBehaviour {
		float PresentTime = 15.0f;
		float NextTime = 15.0f;
		float XessTime = 0.0f;
		int x = 0;
		float ChangeTex = 0.0f;
	// Use this for initialization
	void Start () {
		ChangeTex = (PresentTime - (NextTime/2));
	}
	
	// Update is called once per frame everything done in c#
	void Update () {


	if(x == 0)
	{
		if(Time.timeSinceLevelLoad > XessTime)
		{
			XessTime += 1.0f;
			light.intensity -= (float)(0.6/(NextTime));
		}
		if(Time.timeSinceLevelLoad > ChangeTex)
		{
			GlobalAttributes.DaySwitch = 1;
		}
		if(Time.timeSinceLevelLoad > PresentTime)
		{
			PresentTime += NextTime;
			ChangeTex = (PresentTime - (NextTime/2));
			x=1;
		}
	}
	if(x == 1)
	{	
		if(Time.timeSinceLevelLoad > XessTime)
		{
			XessTime += 1.0f;
			light.intensity += (float)(0.6/(NextTime));
		}
		if(Time.timeSinceLevelLoad > ChangeTex)
		{
			GlobalAttributes.DaySwitch = 0;
		}
		if(Time.timeSinceLevelLoad > PresentTime)
		{
			PresentTime += NextTime;
			ChangeTex = (PresentTime - (NextTime/2));
			x =0;
		}
	
	}
}
}