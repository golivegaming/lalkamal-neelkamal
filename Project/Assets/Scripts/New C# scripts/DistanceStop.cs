using UnityEngine;
using System.Collections;

public class DistanceStop : MonoBehaviour 
{
	static public float HeroDistance = 10.0f;
	static public float HeroAIDistance = 10.0f;
		
	
	void Update () 
	{
		HeroDistance = Vector3.Distance(GameObject.FindWithTag("hero").transform.position, GameObject.FindWithTag("enemy").transform.position);
		HeroAIDistance = Vector2.Distance(GameObject.FindWithTag("hero2").transform.position, GameObject.FindWithTag("enemy").transform.position);
		if(HeroDistance < GlobalAttributes.HeroRange && Attack.FireOK == 0)
		{	
			GlobalAttributes.HeroFireFlag = 0;
			GlobalAttributes.HeroWalkSpeed = 0.0f;
			Attack.FireOK = 1;
		}
		if(HeroDistance < GlobalAttributes.EnemyRange)
		{	
			GlobalAttributes.EnemyFireFlag = 0;
			GlobalAttributes.EnemyWalkSpeed = 0.0f;
		}
		
		if(HeroAIDistance < GlobalAttributes.SecHeroRange)
		{	
			GlobalAttributes.SecHeroFireFlag = 0;
			GlobalAttributes.SecHeroWalkSpeed = 0.0f;
		}
	}
}