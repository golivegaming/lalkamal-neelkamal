using UnityEngine;
using System.Collections;

public class GlobalAttributes : MonoBehaviour 
{
	// Hero attributes used in the game
	static public float HeroRange 		= 2.0f;
	static public float HeroFireSpeed 	= 5.0f;
	static public float HeroWalkSpeed 	= 1.0f;
	static public int 	HeroFireFlag 	= 1;
	static public int 	HeroSpawn		= 0;
	static public int 	HeroHealth 		= 100;
	static public int 	HeroStrength 	= 100;
	
	// Second hero attributes used in th game
	static public float SecHeroRange 		= 3.0f;
	static public float SecHeroFireSpeed 	= 15.0f;
	static public float SecHeroWalkSpeed 	= 1.0f;
	static public int 	SecHeroFireFlag 	= 1;
	static public int 	SecHeroSpawn		= 0;
	static public int 	SecHeroHealth 		= 100;
	static public int 	SecHeroStrength 	= 100;
	
	// Enemy attributes used in the game
	static public float EnemyRange 		= 1.0f;
	static public float EnemyFireSpeed 	= 5.0f;
	static public float EnemyWalkSpeed 	=1.0f;
	static public int 	EnemyFireFlag 	= 1;
	static public int 	EnemyLife 		= 0;
	static public int 	EnemyHealth 	= 100;
	static public int 	EnemyStrength 	= 100;
	
	static public int DaySwitch = 0;
	static public int PathSpawn = 0;
	static public int DaemonGems = 10;
	static public int SpecialPowers = 0;
	static public int GemCount = 10;
	
	static public float LookSpeed = 1.0f;
	
	static public bool isPaused = false;

}
