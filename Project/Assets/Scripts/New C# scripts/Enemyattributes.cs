/*..................
 * TO 			: 	Assign attributes to the Enemies
 	* walk
 	* Attack
 ...............*/

using UnityEngine;
using System.Collections;

public class Enemyattributes : MonoBehaviour 
{
	public Rigidbody EnemyBullet;
	Animation EnemyAnimation;
	public GameObject DaemonGem;
	int Enemy1SP = 0;
	int Enemy2SP = 0;
	int EnemySP = 0;
	
	void Start()
	{
		GlobalAttributes.EnemyHealth = 100;
	}

	void Update () 
	{
		if(!GlobalAttributes.isPaused)
		{
			EnemyAnimation = gameObject.GetComponent("Animation") as Animation;
			//give movement to the hero
			this.gameObject.transform.Translate(Vector3.forward *Time.smoothDeltaTime * GlobalAttributes.EnemyWalkSpeed);
			// Fire from hero seeing towards target
			if(GlobalAttributes.EnemyFireFlag == 0)
			{	
				LookAt(transform.FindChild("firePosition").gameObject, GameObject.FindWithTag("hero").transform.FindChild("Object002").gameObject, GlobalAttributes.LookSpeed);
				FireAt(EnemyBullet, transform.FindChild("firePosition").gameObject, GlobalAttributes.EnemyFireSpeed);
				GlobalAttributes.EnemyFireFlag = 1;
			}
			
			if(DistanceStop.HeroDistance < GlobalAttributes.EnemyRange)
			{
				EnemyAnimation.CrossFade("Attack01");
			}
			else
				EnemyAnimation.CrossFade("Run");
			}

	}
	// To turn according to the target
	void LookAt(GameObject obj, GameObject  targetobj, float  rotationspeed)
	{
		Quaternion rotation = Quaternion.LookRotation(targetobj.transform.position - obj.transform.position);
		obj.transform.rotation = Quaternion.Slerp(obj.transform.rotation, rotation,rotationspeed);
	}
	// To fire towards the target
	void FireAt(Rigidbody projectile, GameObject obj, float  firespeed)
	{	
		Rigidbody clone = (Rigidbody) Instantiate(projectile, obj.transform.position, obj.transform.rotation);
		clone.velocity = obj.transform.forward * firespeed;
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if(!GlobalAttributes.isPaused)
		{
			if(collision.gameObject.tag == "herobullet")
			{
				GlobalAttributes.EnemyHealth -= GlobalAttributes.HeroStrength;
				Destroy(collision.gameObject);
			
			}
		
			if(collision.gameObject.tag == "secherobullet")
			{
				GlobalAttributes.EnemyHealth -= GlobalAttributes.SecHeroStrength;
				Destroy(collision.gameObject);
				
			}
			
			if(GlobalAttributes.EnemyHealth == 0)
			{
				Player1attributes.PlayFire = 0;
				Player2attributes.SecPlayFire = 0;
				Destroy(this.gameObject);
				GlobalAttributes.HeroWalkSpeed = 1.0f;
				GlobalAttributes.EnemyWalkSpeed = 1.0f;
				GlobalAttributes.SecHeroWalkSpeed = 1.0f;
				GlobalAttributes.EnemyLife = 0;
				Player1attributes.PlayFire = 0;
				Player2attributes.SecPlayFire = 0;
				Instantiate(DaemonGem, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
		
			}
		}
		
	}
}

// Animations0