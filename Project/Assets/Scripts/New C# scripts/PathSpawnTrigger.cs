using UnityEngine;
using System.Collections;

public class PathSpawnTrigger : MonoBehaviour {
	
	public Transform[] prefab = new Transform[5];
	Vector3 Position;
	
	static public GameObject ObjectDestroy;
	static public int i = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "hero")
		{
			//savepointfire.visible = 1;		
			i=Random.Range(0,prefab.Length);
			ObjectDestroy = this.transform.parent.gameObject;
			Position = new Vector3(transform.position.x+30.0f, transform.position.y, transform.position.z);
			//if(GlobalAttributes.PathSpawn == 0)
			//{
				Instantiate (prefab[i], Position, transform.rotation);
				//GlobalAttributes.PathSpawn = 1;
			//}
	}
	}
}
