/*..................
 * TO 		: 	Assign attributes to the player2 
 	* walk
 	* Attack
 ...............*/

using UnityEngine;
using System.Collections;

public class Player2attributes : MonoBehaviour 
{
	public Rigidbody SecHeroBullet;
	Animation HeroaiAnimation;
	static public int SecPlayFire = 0; 

	void Update () 
	{
		if(!GlobalAttributes.isPaused)
		{
			HeroaiAnimation = gameObject.GetComponent("Animation") as Animation;
			//give movement to the hero
			this.gameObject.transform.Translate(Vector3.forward *Time.smoothDeltaTime * GlobalAttributes.SecHeroWalkSpeed);
			// Fire from hero seeing towards target
			if(GlobalAttributes.SecHeroFireFlag == 0 /*&& SecPlayFire == 0*/)
			{	
				HeroaiAnimation.CrossFade("attackhero2");
				//LookAt(transform.FindChild("Fireposition").gameObject, GameObject.FindWithTag("enemy").transform.FindChild("Bip001").gameObject, GlobalAttributes.LookSpeed);
				//FireAt(SecHeroBullet, transform.FindChild("Fireposition").gameObject, GlobalAttributes.SecHeroFireSpeed);
				GlobalAttributes.SecHeroFireFlag = 1;
				SecPlayFire = 1;
			}
			else
				HeroaiAnimation.CrossFade("runhero2");
		}
	}
	// To turn according to the target
	void LookAt(GameObject obj, GameObject  targetobj, float  rotationspeed)
	{
		Quaternion rotation = Quaternion.LookRotation(targetobj.transform.position - obj.transform.position);
		obj.transform.rotation = Quaternion.Slerp(obj.transform.rotation, rotation,rotationspeed);
	}
	// To fire towards the target
	void FireAt(Rigidbody projectile, GameObject obj, float  firespeed)
	{
		Rigidbody clone = (Rigidbody) Instantiate(projectile, obj.transform.position, obj.transform.rotation);
		clone.velocity = obj.transform.forward * firespeed;
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if(!GlobalAttributes.isPaused)
		{
			if(collision.gameObject.tag == "enemybullet")
			{
				GlobalAttributes.SecHeroHealth -= GlobalAttributes.EnemyStrength;
				Destroy(collision.gameObject);
			}
			
			if(GlobalAttributes.SecHeroHealth == 0)
			{
				Destroy(this.gameObject);
			}
		}
	}
}
