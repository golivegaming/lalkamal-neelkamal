using UnityEngine;
using System.Collections;

public class GemCollision : MonoBehaviour 
{
	UILabel _DemGemCount;
	
	void Start()
	{
		_DemGemCount = GameObject.Find("DemGemCount").GetComponent<UILabel>();;
	}
	
	void OnCollisionEnter(Collision col)
	{
		
		if(col.gameObject.tag == "hero")
		{
			Destroy(this.gameObject);
			GlobalAttributes.DaemonGems += GlobalAttributes.GemCount;
			_DemGemCount.text = ""+ (GlobalAttributes.DaemonGems);
		}
		
		/*if(col.gameObject.tag == "hero2")
		{
			Destroy(this.gameObject);
			GlobalAttributes.DaemonGems += GlobalAttributes.GemCount;
		}*/
	}
}
