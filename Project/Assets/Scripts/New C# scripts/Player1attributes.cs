/*..................
 * TO 			: 	Assign attributes to the player2 
 	* walk
 	* Attack
 	* Enemy spawn
 ...............*/

using UnityEngine;
using System.Collections;

public class Player1attributes : MonoBehaviour 
{
	public Rigidbody HeroBullet;
	public Rigidbody[] EnemyPrefab = new Rigidbody[3];
	Animation HeroAnimation;
	static public int PlayFire = 0; 
	void Update () 
	{
		if(!GlobalAttributes.isPaused)
		{
			HeroAnimation = gameObject.GetComponent("Animation") as Animation;
			//give movement to the hero
			this.gameObject.transform.Translate(Vector3.forward *Time.smoothDeltaTime * GlobalAttributes.HeroWalkSpeed);
			// Initlize enemy at particular distance from hero
			if(GlobalAttributes.EnemyLife == 0)
			{	
				Rigidbody clone = (Rigidbody)Instantiate(EnemyPrefab[Random.Range(0, EnemyPrefab.Length)], transform.FindChild("Enemyspawn position").transform.position, transform.FindChild("Enemyspawn position").transform.rotation);
				GlobalAttributes.EnemyLife = 1;			
			}
			// Fire from hero seeing towards target
			if(GlobalAttributes.HeroFireFlag == 0)
			{	
				LookAt(transform.FindChild("Fireposition").gameObject, GameObject.FindWithTag("enemy").transform.FindChild("Bip001").gameObject, GlobalAttributes.LookSpeed);
				FireAt(HeroBullet, transform.FindChild("Fireposition").gameObject, GlobalAttributes.HeroFireSpeed);
				GlobalAttributes.HeroFireFlag = 1;
			}
			
			if(DistanceStop.HeroDistance < GlobalAttributes.HeroRange)
			{
				HeroAnimation.CrossFade("attackhero1");
			}
			else
				HeroAnimation.CrossFade("runhero1");
		}

	}
	// To turn according to the target
	void LookAt(GameObject obj, GameObject  targetobj, float  rotationspeed)
	{
		Quaternion rotation = Quaternion.LookRotation(targetobj.transform.position - obj.transform.position);
		obj.transform.rotation = Quaternion.Slerp(obj.transform.rotation, rotation,rotationspeed);
	}
	// To fire towards the target
	void FireAt(Rigidbody projectile, GameObject obj, float  firespeed)
	{
		Rigidbody clone = (Rigidbody) Instantiate(projectile, obj.transform.position, obj.transform.rotation);
		clone.velocity = obj.transform.forward * firespeed;
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if(!GlobalAttributes.isPaused)
		{
			if(collision.gameObject.tag == "enemybullet")
			{
				GlobalAttributes.HeroHealth -= GlobalAttributes.EnemyStrength;
				Destroy(collision.gameObject);
			}
			
			if(GlobalAttributes.HeroHealth == 0)
			{
				Destroy(this.gameObject);
				Destroy(GameObject.FindWithTag("hero2"));
				Destroy(GameObject.FindWithTag("enemy"));
			}
		}
	}
}
