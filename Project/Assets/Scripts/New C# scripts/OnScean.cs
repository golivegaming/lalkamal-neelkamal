using UnityEngine;
using System.Collections;

public class OnScean : MonoBehaviour {
	public GameObject Hero;
	public GameObject SecHero;
	
	public Texture2D Layer1Day;
	public Texture2D Layer1Night;
	public Texture2D Layer2Day;
	public Texture2D Layer2Night;
	public Texture2D Layer3Day;
	public Texture2D Layer3Night;
	public Texture2D Layer4Day;
	public Texture2D Layer4Night;
	
	static public int LitFireFlag = 1;
	Vector3 ObjectPosition;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(GlobalAttributes.HeroSpawn == 0)
		{
			Instantiate(Hero, transform.FindChild("Herospawnpos").transform.position, transform.FindChild("Herospawnpos").transform.rotation);
			GlobalAttributes.HeroSpawn = 1;
		}
		
		if(GlobalAttributes.SecHeroSpawn == 0)
		{
			Instantiate(SecHero, transform.FindChild("secHerospawnpos").transform.position, transform.FindChild("secHerospawnpos").transform.rotation);
			GlobalAttributes.SecHeroSpawn = 1;
		}
		
		ObjectPosition = transform.position;
		if(GameObject.Find("Main Camera").transform.position.x < (ObjectPosition.x + 25))
		{
			//Debug.Log("Fine");
		}
		else
		{
		Destroy(PathSpawnTrigger.ObjectDestroy);
		}
		
		if(GlobalAttributes.DaySwitch == 0)
		{
			transform.FindChild("Layer0").renderer.material.mainTexture = Layer1Day;
			transform.FindChild("Layer1").renderer.material.mainTexture = Layer2Day;
			transform.FindChild("Layer2").renderer.material.mainTexture = Layer3Day;
			transform.FindChild("Layer3").renderer.material.mainTexture = Layer4Day;
		}
		else
		{
			transform.FindChild("Layer0").renderer.material.mainTexture = Layer1Night;
			transform.FindChild("Layer1").renderer.material.mainTexture = Layer2Night;
			transform.FindChild("Layer2").renderer.material.mainTexture = Layer3Night;
			transform.FindChild("Layer3").renderer.material.mainTexture = Layer4Night;
		}
		
		if(LitFireFlag == 0)
		{
			transform.FindChild("Savepointindi").renderer.enabled = true;
		}
		else
		{
			transform.FindChild("Savepointindi").renderer.enabled = false;
		}
	}
}
