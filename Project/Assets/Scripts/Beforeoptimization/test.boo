import UnityEngine
 
class Vibration(MonoBehaviour):
	private viblator as AndroidJavaObject
 
	def Start():
		unityPlayer as AndroidJavaClass = AndroidJavaClass("com.unity3d.player.UnityPlayer")
		currentActivity as AndroidJavaObject = unityPlayer.GetStatic[of AndroidJavaObject]("currentActivity")
		viblator = currentActivity.Call[of AndroidJavaObject]("getSystemService","vibrator")
 
	def _Dummy():
		Handheld.Vibrate()
 
	def Vibator(time as long):
		viblator.Call("vibrate", time)