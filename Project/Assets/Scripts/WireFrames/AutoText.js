var letterPause = 0.2;
var sound : AudioClip;
 
 
var myText : String = "GoLive Gaming Studios";
 
function Start () 
{       
	TypeText();
}
 
function TypeText() 
{
    for (var letter in myText.ToCharArray())
    {
        guiText.text += letter;
        if (sound)
        	audio.PlayOneShot(sound);
        yield WaitForSeconds (letterPause);
	}              
	GameObject.Find("tap").guiText.text = "Tap to Continue";
}