using UnityEngine;
using System.Collections;

public class HealthBarOnHead : MonoBehaviour 
{
	float health = 0;
	bool change = false;
	
	void Update()
	{
		if(health > 1)
			change = true;
		if(health < 0)
			change = false;
		if(health>0 && change)
		{
			health-=0.01f;
		}
		if(health<1 && !change)
		{
			health+=0.01f;
		}
		transform.localScale = new Vector3(health, transform.localScale.y, transform.localScale.z);
	}
}
