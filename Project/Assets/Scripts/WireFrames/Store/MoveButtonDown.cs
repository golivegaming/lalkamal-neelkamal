using UnityEngine;
using System.Collections;

public class MoveButtonDown : MonoBehaviour 
{
 	public UIButton _Weapons;
	public UIButton _HeroPowers;
	public UIButton _HealingPortion;
	public UISprite _PopupSprite;
	
	private bool isClicked = false;
	
	
	
	void start()
	{
		//_Weapons = GameObject.Find("Weapons").GetComponent<UIButton>();
		//_HeroPowers = GameObject.Find("Hero Powers").GetComponent<UIButton>();
	}
	
	void OnClick()
	{
		isClicked = !isClicked;
		if(isClicked)
		{
			TweenPosition.Begin(_Weapons.gameObject, 0.1f, new Vector3(-410,-204, -20));
			TweenPosition.Begin(_HeroPowers.gameObject, 0.1f, new Vector3(-410,-264,-20));
			TweenPosition.Begin(_HealingPortion.gameObject, 0.1f, new Vector3(-410, -334, -20));
			TweenPosition.Begin(_PopupSprite.gameObject, 0.1f, new Vector3(-409, -73, 0));
		}
		if(!isClicked)
		{	
			TweenPosition.Begin(_PopupSprite.gameObject, 0.1f, new Vector3(-960, -73, 0));
			TweenPosition.Begin(_Weapons.gameObject, 0.1f, new Vector3(-410,45,0));
			TweenPosition.Begin(_HeroPowers.gameObject, 0.1f, new Vector3(-410,-40,0));
			TweenPosition.Begin(_HealingPortion.gameObject, 0.1f, new Vector3(-410, -105, 0));	

		}
		
	}
}
