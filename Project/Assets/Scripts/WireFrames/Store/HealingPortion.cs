using UnityEngine;
using System.Collections;

public class HealingPortion: MonoBehaviour 
{
	
	public UILabel _Info;
	
	public GameObject _HealingPortionSlider;
	
	void OnClick()
	{
		_Info.text = "You just clicked on HealingPortion Button";
		
		if(GameObject.Find("DemGemSlider(Clone)"))
				Destroy(GameObject.Find("DemGemSlider(Clone)"));
				
		if(!GameObject.Find("HealingPortion(Clone)"))
		{
			var clone = NGUITools.AddChild(GameObject.Find("Panel"), _HealingPortionSlider);
			TweenPosition.Begin(clone.gameObject, 0.1f, new Vector3(180, -29, -50));
		}
	}
}
