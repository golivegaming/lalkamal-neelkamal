using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour 
{
	
	public UILabel _Info;
	private bool isClicked = false;
	public GameObject _WeaponSlider;
	
	void OnClick()
	{
		_Info.text = "You just clicked on Weapons Button";
		
		isClicked = !isClicked;
		
		if(isClicked)
		{
			if(GameObject.Find("HealingPortion(Clone)"))
				Destroy(GameObject.Find("HealingPortion(Clone)"));
			if(GameObject.Find("DemGemSlider(Clone)"))
				Destroy(GameObject.Find("DemGemSlider(Clone)"));
			TweenPosition.Begin(_WeaponSlider.gameObject, 0.1f, new Vector3(0, -367, -50));
		}
		if(!isClicked)
		{
			TweenPosition.Begin(_WeaponSlider.gameObject, 0.01f, new Vector3(-386, -367, -50));			
		}
	}
}
