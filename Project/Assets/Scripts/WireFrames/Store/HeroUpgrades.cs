using UnityEngine;
using System.Collections;

public class HeroUpgrades : MonoBehaviour {

	public UILabel _Info;
	public GameObject _HeroPower;
	public GameObject _HealingPortion;
	public GameObject _HealingPortionPopup;
	public GameObject _Weapons;
	public GameObject _WeaponPopup;
	
	public GameObject _HeroUpgradePopup;
	
	private bool isClicked = false;
	
	void OnClick()
	{
		_Info.text = "You just clicked on Hero Upgrades Button";
		
		isClicked = !isClicked;
		
		if(isClicked)
		{
			
			if(GameObject.Find("HealingPortion(Clone)"))
				Destroy(GameObject.Find("HealingPortion(Clone)"));
			if(GameObject.Find("DemGemSlider(Clone)"))
				Destroy(GameObject.Find("DemGemSlider(Clone)"));
			
			TweenPosition.Begin(_HeroPower.gameObject, 0.01f, new Vector3(0, -317, -5));
			TweenPosition.Begin(_HealingPortion.gameObject, 0.01f, new Vector3(0, -377, -5));
			TweenPosition.Begin(_HealingPortionPopup.gameObject, 0.01f, new Vector3(-386, -300, 0));
			TweenPosition.Begin(_Weapons.gameObject, 0.01f, new Vector3(0, -437, -5));
			TweenPosition.Begin(_WeaponPopup.gameObject, 0.01f, new Vector3(-386, -367, 0));
			TweenPosition.Begin(_HeroUpgradePopup.gameObject, 0.1f, new Vector3(0, -188, 0));
		}
		if(!isClicked)
		{
			TweenPosition.Begin(_HeroUpgradePopup.gameObject, 0.01f, new Vector3(-386, -188, 0));
			TweenPosition.Begin(_HeroPower.gameObject, 0.1f, new Vector3(0, -120, -5));
			TweenPosition.Begin(_HealingPortion.gameObject, 0.1f, new Vector3(0, -180, -5));
			TweenPosition.Begin(_HealingPortionPopup.gameObject, 0.1f, new Vector3(-386, -300, 0));
			TweenPosition.Begin(_Weapons.gameObject, 0.1f, new Vector3(0, -240, -5));
			TweenPosition.Begin(_WeaponPopup.gameObject, 0.1f, new Vector3(-386, -367, 0));
		}
	}
}
