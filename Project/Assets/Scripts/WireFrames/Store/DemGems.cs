using UnityEngine;
using System.Collections;

public class DemGems : MonoBehaviour 
{
	public UILabel _DemGemCount;
	
	public GameObject _DemGemSlider;
	public UILabel _Info;
	
	private bool isClicked = false;
	
	void Start()
	{
		_DemGemCount = GameObject.Find("DemGemCount").GetComponent<UILabel>();
		_DemGemCount.text = " " + GlobalAttributes.DaemonGems;
		var clone = NGUITools.AddChild(GameObject.Find("Panel"), _DemGemSlider);
		TweenPosition.Begin(clone.gameObject, 0.1f, new Vector3(180, -29, -50));
	}
	
	void OnClick()
	{
		_Info.text = "You just clicked on DemGems Button";
		
		isClicked = !isClicked;
		
		if(isClicked)
		{
			if(GameObject.Find("HealingPortion(Clone)"))
				Destroy(GameObject.Find("HealingPortion(Clone)"));
			
			if(!GameObject.Find("DemGemSlider(Clone)"))
			{
				var clone = NGUITools.AddChild(GameObject.Find("Panel"), _DemGemSlider);
				TweenPosition.Begin(clone.gameObject, 0.1f, new Vector3(180, -29, -50));
			}
		}
		if(!isClicked)
		{
			//Destroy(GameObject.Find("DemGemSlider(Clone)"));
			
			/*TweenPosition.Begin(_DemGemSlider.gameObject, 0.01f, new Vector3(1042, -29, -50));
			
			Vector4 reset = _DemGemSlider.clipRange;
			reset.x = 0;
			reset.y = 0;
			_DemGemSlider.clipRange = reset;*/
			
		}
	}
}
