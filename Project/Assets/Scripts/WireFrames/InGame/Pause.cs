using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour 
{	
	private bool isClicked = false;
	void Start()
	{
		GameObject.Find("PauseCamera").GetComponent<Camera>().cullingMask = 0;
		
	}
	void OnClick()
	{
		isClicked = !isClicked;
		if(isClicked)
		{
			/* Stop Walking
			GlobalAttributes.HeroWalkSpeed = 0.0f;
			GlobalAttributes.SecHeroWalkSpeed = 0.0f;
			GlobalAttributes.EnemyWalkSpeed = 0.0f;
			
			//Stop Firing
			GlobalAttributes.HeroFireFlag = 1;
			GlobalAttributes.EnemyFireFlag = 1;
			GlobalAttributes.SecHeroFireFlag = 1;*/
			
			GlobalAttributes.isPaused = true;
			
			GameObject.Find("PauseCamera").GetComponent<Camera>().cullingMask = (1 << LayerMask.NameToLayer("Pause"));
		}
		if(!isClicked)
		{
			GameObject.Find("PauseCamera").GetComponent<Camera>().cullingMask = 0;
			
			GlobalAttributes.isPaused = false;
			
			/* Start Walking
			GlobalAttributes.HeroWalkSpeed = 1.0f;
			GlobalAttributes.SecHeroWalkSpeed = 1.0f;
			GlobalAttributes.EnemyWalkSpeed = 1.0f;
			
			//Start Firing
			GlobalAttributes.HeroFireFlag = 0;
			GlobalAttributes.EnemyFireFlag = 0;
			GlobalAttributes.SecHeroFireFlag = 0;*/
			
		}
	}
}
