using UnityEngine;
using System.Collections;

public class WeaponClick : MonoBehaviour 
{
	private bool isClicked = false;
	void Start()
	{
		GameObject.Find("WeaponCamera").GetComponent<Camera>().cullingMask = 0;
		
	}
	void OnClick()
	{
		isClicked = !isClicked;
		if(isClicked)
			GameObject.Find("WeaponCamera").GetComponent<Camera>().cullingMask = (1 << LayerMask.NameToLayer("Weapon"));
		if(!isClicked)
			GameObject.Find("WeaponCamera").GetComponent<Camera>().cullingMask = 0;
	}
	
}
