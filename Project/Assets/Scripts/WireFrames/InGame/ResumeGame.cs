using UnityEngine;
using System.Collections;

public class ResumeGame : MonoBehaviour 
{
	void OnClick()
	{
		
		GameObject.Find("PauseCamera").GetComponent<Camera>().cullingMask = 0;
		
		// Start Walking
			GlobalAttributes.HeroWalkSpeed = 1.0f;
			GlobalAttributes.SecHeroWalkSpeed = 1.0f;
			GlobalAttributes.EnemyWalkSpeed = 1.0f;
			
			//Start Firing
			GlobalAttributes.HeroFireFlag = 0;
			GlobalAttributes.EnemyFireFlag = 0;
			GlobalAttributes.SecHeroFireFlag = 0;
	}
}
