using UnityEngine;
using System.Collections;

public class StoryBoard : MonoBehaviour 
{
	string[] SceneNames = new string[]{"Storyboard_Fram_00", "Storyboard_Fram_01", "Storyboard_Fram_02", "Storyboard_Fram_03", "Storyboard_Fram_04", "Storyboard_Fram_05", "Storyboard_Fram_06", "Storyboard_Fram_07", "Storyboard_Fram_08", "Storyboard_Fram_09"};
	int i=0;
	UISprite _BoardPanel;
	
	void Start()
	{
		_BoardPanel = GameObject.Find("StoryBoard").GetComponent<UISprite>();
		StorySequence();
	}

	void StorySequence()
	{	
		Debug.Log("Seq");
		_BoardPanel.spriteName = SceneNames[i++];
		Debug.Log (i);
		if(i<10)
			Invoke("StorySequence", 2);
		if(i==10 && Story.FromHomeMenu)
			Application.LoadLevel("HomeMenuScreen");
		if(i==10 && ChoosePlayer.FromEpicJourney)
			Application.LoadLevel("Map");
	}
	
	void OnClick()
	{
		if(Story.FromHomeMenu)
		{
			Application.LoadLevel("HomeMenuScreen");
		}
		if(ChoosePlayer.FromEpicJourney)
		{
			Application.LoadLevel("Map");
		}
	}
}
